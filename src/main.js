/* jshint browser: true */

(function () {
  // We'll copy the properties below into the mirror div.
  // Note that some browsers, such as Firefox, do not concatenate properties
  // into their shorthand (e.g. padding-top, padding-bottom etc. -> padding),
  // so we have to list every single property explicitly.
  var properties = [
    'direction', // RTL support
    'boxSizing',
    'width',
    'height',
    'overflowX',
    'overflowY',

    'borderTopWidth',
    'borderRightWidth',
    'borderBottomWidth',
    'borderLeftWidth',
    'borderStyle',

    'paddingTop',
    'paddingRight',
    'paddingBottom',
    'paddingLeft',

    'fontStyle',
    'fontVariant',
    'fontWeight',
    'fontStretch',
    'fontSize',
    'fontSizeAdjust',
    'lineHeight',
    'fontFamily',

    'textAlign',
    'textTransform',
    'textIndent',
    'textDecoration',

    'letterSpacing',
    'wordSpacing',

    'tabSize',
    'MozTabSize',
  ];

  function getCaretCoordinates(element, position, cursor) {

    // The mirror div will replicate the textarea's style
    var div = document.createElement('div');
    document.body.appendChild(div);

    var style = div.style;
    var computed = window.getComputedStyle ? window.getComputedStyle(element) : element.currentStyle; // currentStyle for IE < 9

    style.whiteSpace = 'pre-wrap';
    style.position = 'absolute';

    function getLineHeight() {
      var lineHeight = parseInt(computed.lineHeight);
      if (isNaN(lineHeight)) {
        lineHeight = parseInt(computed.fontSize);
      }
      return lineHeight;
    }

    // Transfer the element's properties to the div
    properties.forEach(function (prop) {
      if (prop === 'lineHeight') {
        // Special case for <input>s because text is rendered
        // centered and line height may be != height
        if (computed.boxSizing === 'border-box') {
          var height = parseInt(computed.height);
          var lineHeight = getLineHeight();
          var outerHeight =
            parseInt(computed.paddingTop) +
            parseInt(computed.paddingBottom) +
            parseInt(computed.borderTopWidth) +
            parseInt(computed.borderBottomWidth);
          var targetHeight = outerHeight + lineHeight;
          if (height > targetHeight) {
            style.lineHeight = height - outerHeight + 'px';
          } else if (height === targetHeight) {
            style.lineHeight = lineHeight;
          } else {
            style.lineHeight = 0;
          }
        } else {
          style.lineHeight = computed.height;
        }
      } else {
        style[prop] = computed[prop];
      }

      if (prop !== 'width' && prop !== 'height' && !prop.startsWith('padding') && !prop.startsWith('border')) {
        cursor.style[prop] = computed[prop];
      }
    });

    cursor.style.width = (parseInt(computed.fontSize) / 2) + 5 + 'px';
    cursor.style.minWidth = (parseInt(computed.fontSize) / 2) + 5 + 'px';
    input.style.caretColor = 'transparent';

    div.textContent = element.value.substring(0, position);
    div.textContent = div.textContent.replace(/\s/g, '\u00a0');

    var span = document.createElement('span');
    span.textContent = element.value.substring(position) || '';
    if (!div.textContent && !span.textContent) {
      span.textContent = 'a';
    }
    div.appendChild(span);

    var coordinates = {
      top: span.offsetTop + parseInt(computed['borderTopWidth']) + parseInt(computed['paddingTop']),
      left: span.offsetLeft + parseInt(computed['borderLeftWidth']),
      height: getLineHeight(),
    };

    document.body.removeChild(div);

    return coordinates;
  }

  var input = document.querySelector('input[data-input-block-caret]');
  var cursor = document.createElement('div');

  function interpret() {
    var value = input.value.trim();
    window.location.href = 'https://google.com/search?q=' + encodeArgs(value);
  }
  function encodeArgs(e, t) {
    if ((void 0 === t && (t = !1), t)) for (o = 0; o < e.length; o++) e[o] = e[o].replace(/ /g, '+');
    else for (var o = 0; o < e.length; o++) e[o] = encodeURIComponent(e[o]);
    return e;
  }

  function update() {
    var value = input.value[input.selectionEnd] || ' ';
    if (value === undefined) {
      value = ' ';
    }
    var coordinates = getCaretCoordinates(input, input.selectionEnd, cursor);
    cursor.style.top = input.offsetTop - input.scrollTop + coordinates.top + 'px';
    cursor.style.left = input.offsetLeft - input.scrollLeft + coordinates.left + 'px';
    cursor.textContent = value.replace(/\s/g, '\u00a0');
  }

  function handleKeyDown(e) {
    if ((e.which || e.keyCode) === 13) {
      return interpret();
    }
  }

  window.onload = function () {

    document.body.appendChild(cursor);
    cursor.style.position = 'absolute';
    cursor.style.background = 'white';
    cursor.style.color = 'black';

    input.addEventListener('keydown', handleKeyDown);
    input.parentNode.appendChild(cursor);

    ['keydown', 'click', 'scroll'].forEach(function (event) {
      input.addEventListener(event, () => {
        setTimeout(() => update());
      });
    });
    setTimeout(() => update());

    document.body.addEventListener('click', function () {
      input.focus();
    });
  };

})();
